/*
 * Copyright (C) 2013 Wolfram Rittmeyer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.grokkingandroid.sampleapp.samples.actionbar.actionviews;

import android.annotation.TargetApi;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.os.Build;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.grokkingandroid.sampleapp.samples.actionbar.actionviews.R;


@TargetApi(Build.VERSION_CODES.FROYO)
public class SearchViewFragment extends DemoBaseFragment {

   @Override
   public String getDescriptionText() {
      return getResources().getString(R.string.searchview_desc);
   }

   @Override
   public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
      inflater.inflate(R.menu.menu_fragment_searchview, menu);
      MenuItem item = menu.findItem(R.id.searchView);
      
      SearchView searchView = (SearchView)item.getActionView();
      
      SearchManager searchManager = (SearchManager)getSherlockActivity().getSystemService(Context.SEARCH_SERVICE);
      SearchableInfo info = searchManager.getSearchableInfo(getSherlockActivity().getComponentName());
      searchView.setSearchableInfo(info);      
   }
   
   public static SearchViewFragment newInstance() {
	   return new SearchViewFragment();
   }

   @Override
   public String[] getLinkTexts() {
      return getResources().getStringArray(R.array.searchview_link_texts);
   }

   @Override
   public String[] getLinkTargets() {
      return getResources().getStringArray(R.array.searchview_link_targets);
   }

}
