package com.grokkingandroid.sampleapp.samples.actionbar.actionviews;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.grokkingandroid.sampleapp.samples.actionbar.actionviews.R;


public abstract class DemoBaseFragment extends SherlockFragment {

   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setHasOptionsMenu(true);
   }

   @SuppressWarnings("deprecation")
   @SuppressLint("NewApi")
   protected void showLinks(ViewGroup container) {      
      String[] linkTexts = getLinkTexts();
      String[] linkTargets = getLinkTargets();
      StringBuilder link = null;
      for (int i = 0; i < linkTexts.length; i++)  {
         // TODO: Use an inflater
         TextView tv = new TextView(getSherlockActivity());
         LinearLayout.LayoutParams params = null;
         params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
         if (i != linkTexts.length -1) {
            int marginBottom = getResources().getDimensionPixelSize(R.dimen.linkSpacing);
            params.setMargins(0,  0, 0, marginBottom);
         }
         tv.setLayoutParams(params);
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            tv.setTextIsSelectable(true);
         }
         tv.setTextAppearance(getSherlockActivity(), android.R.style.TextAppearance_Medium);
         link = new StringBuilder(100);
         link.append("<a href=\"");
         link.append(linkTargets[i]);
         link.append("\">");
         link.append(linkTexts[i]);
         link.append("</a>");
         Spanned spannedLink = Html.fromHtml(link.toString());
         tv.setText(spannedLink);
         tv.setMovementMethod(LinkMovementMethod.getInstance());
         container.addView(tv);
      }
   }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
         Bundle savedInstanceState) {
      View rootView = inflater.inflate(R.layout.fragment_demo_base,
            container, false);
      TextView description = (TextView) rootView
            .findViewById(R.id.demoapp_fragment_description);
      Spanned descSpannable= Html.fromHtml(getDescriptionText());
      description.setText(descSpannable);
      description.setMovementMethod(LinkMovementMethod.getInstance());
      ViewGroup linkContainer = (ViewGroup)rootView.findViewById(R.id.container_demo_blog_links);
      showLinks(linkContainer);
      linkContainer.invalidate();
      // delegate to subclass for content view
      ViewGroup contentContainer = 
            (ViewGroup)rootView.findViewById(R.id.container_demo_content);
      onCreateContentView(inflater, contentContainer, savedInstanceState);
      return rootView;
   }

   public String[] getLinkTexts() {
      return getResources().getStringArray(R.array.default_link_texts);
   }
   
   public String[] getLinkTargets() {
      return getResources().getStringArray(R.array.default_link_targets);
   }
   
   public void onCreateContentView(LayoutInflater inflater, 
         ViewGroup container, Bundle savedInstanceState) {
      //TODO: temporary solution;
      // either generate it or do it in a better way
      // works for now, though
      container.setVisibility(View.GONE);
   }

   public abstract String getDescriptionText();
   
}