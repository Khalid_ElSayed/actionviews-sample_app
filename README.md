#Readme
This is a sample project to showcase ActionViews. ActionViews are custom views you can use within the ActionBar. 

This demo makes of of ActionBarSherlock **and thus you have to add ActionBarSherlock as a project dependency**.

Note: You might have to clean your project to get rid of Lint warnings and errors.


##Most important classes
For using the SearchView see `SearchViewFragment.java` together with the menu file `menu_fragment_searchview.xml`.

If you want to inline replace an ActionBar item refer to `PlaceholderActionViewFragment.java` and the accompanying menu definition in `menu_fragment_placeholder.xml`.

To understand the preexpanded layout see `PreExpandedActionViewFragment.java` together with the menu xml definition in `menu_fragment_expandable.xml`.

Finally if you're interested in expandable Action Items you should look at `ExpandableActionViewFragment.java` and the menu xml definition in `menu_fragment_expandable.xml`.


##Screenshot
[![Example Image](http://www.grokkingandroid.com/wordpress/wp-content/uploads/2013/04/actionviews_demoapp_searchview_unexpanded-153x300.png)](http://www.grokkingandroid.com/wordpress/wp-content/uploads/2013/04/actionviews_demoapp_searchview_unexpanded.png)


##Relevant Blogposts on [Grokking Android](http://www.grokkingandroid.com/)
[Adding ActionViews to Your ActionBar](http://www.grokkingandroid.com/adding-actionviews-to-your-actionbar/)

[Adding ActionBarSherlock to Your Project](http://www.grokkingandroid.com/adding-actionbarsherlock-to-your-project/)

[Getting Started With Fragments and the Support Library](http://www.grokkingandroid.com/getting-started-with-fragments-and-the-support-library/)


##Warning: Generated codebase

**Please note:** I have created the core of this sample using a custom generator. I'm going to publish many samples when writing blog posts and want to keep my work for those as focused as possible. The generator helps me achieve this even if it means that some code might seem a bit odd. I always refer to the relevant classes at the beginning of this README file.

**tl;dr:** Do not copy without thinking - should go without saying, but it's better to mention it once more!


##Developed by

*Wolfram Rittmeyer* - You can contact me via:

* [Grokking Android (my blog)](http://www.grokkingandroid.com)

* [Google+](https://plus.google.com/101948439228765005787) or

* [Twitter](https://twitter.com/RittmeyerW)


##Thanks
Thanks to all my readers and blog commenters. Your feedback helps me to stay on track and to go on with projects like this.

Special thanks also to the Android crowd on G+. You're an awesome crowd. I have gained many insights by reading posts there, by following links to blog posts or by discussions on G+! 

This readme was created using [dillinger.io](http://dillinger.io). Thanks for this service.


##License
    Copyright 2013 Wolfram Rittmeyer

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

